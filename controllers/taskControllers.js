const Task = require("../models/task");

// Controller function for getting all the tasks
module.exports.getAllTasks = () => {
	return Task.find({}).then(result => {
		return result;
	})
}

// Controller function for creating a task
module.exports.createTask = requestBody => {
	// Create a task object based on the mongoose model "Task"
	let newTask = new Task({
		// Sets the "name" property with the value received from the client
		name : requestBody.name
	})

	return newTask.save().then((task, error) => {
		if(error){
			console.log(error);
			return false;
		}
		// Save is successful
		else{
			return task;
		}
	})
}

// Delete Task Controller
module.exports.deleteTask = taskId =>{
	return Task.findByIdAndRemove(taskId).then((removedTask,error)=>{
		if(err){
			console.log(error)
			return false
		} else{
			return removedTask
		}
	})
}

// COntroller function for updating a task
module.exports.updateTask = (taskId, newContent) => {
	return Task.findById(taskId).then((result,error)=>{
		if(error){
			console.log(error)
			return false
		} 
		result.name = newContent.name;
		return result.save().then((updateTask,saveErr)=>{
			if(saveErr){
				console.log(saveErr)
				return false
			} else{return updateTask}
		})
	})
}

//*********************************ACTIVITY 6/8/22
module.exports.getSpecificTask = taskId =>{
	return Task.findById(taskId).then(result=> {return result})
}

module.exports.updateTaskStatus = (taskId, newContent)=>{
	return Task.findById(taskId).then((result,error)=>{
		if(error){
			console.log(error)
			return false
		}
		result.status = "Completed"
		return result.save().then((updateTask,saveErr)=>{
			if(saveErr){
				console.log(saveErr)
				return false
			} else{
				return updateTask
			}
		})
	})
}