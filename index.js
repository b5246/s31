const express = require("express")
const mongoose = require("mongoose")

const app = express()
const port = 3001
const taskRoute = require("./routes/taskRoute")

app.use(express.json())
app.use(express.urlencoded({extended:true}))


mongoose.connect("mongodb+srv://admin:admin@wdc028-course-booking.6vp9f.mongodb.net/b177-to-do?retryWrites=true&w=majority",{
	useNewUrlParser:true,
	useUnifiedTopology: true
})

app.listen(port,()=>console.log(`Server running at port ${port}`))

let db = mongoose.connection;
db.on("error",console.error.bind(console,"connection error"))
db.once("open",()=>console.log("We're connected to the cloud database"))


app.use("/tasks", taskRoute)
